module.exports = {
  transform: {
    '\\.json$': './tools/jsonTransformer',
    '\\.js$': 'babel-jest',
    '.*': './tools/emptyTransformer',
  },
  collectCoverageFrom: ['**/*.{ts,tsx,js,jsx}'],
  testMatch: ['**/__tests__/**/*.test.js'],
  testPathIgnorePatterns: ['/node_modules/', '/__mocks__/'],
  modulePathIgnorePatterns: ['/lib/', '/__mocks__/'],
  coverageDirectory: 'coverage',
  setupFiles: ['./tools/mockStorages'],
  testURL: 'http://localhost/',
  cacheDirectory: '.cache/jest',
  roots: [''],
}
