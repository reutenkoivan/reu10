module.exports = {
  env: {
    browser: true,
    commonjs: true,
    node: true,
    es6: true,
  },
  extends: ['eslint:recommended', 'plugin:import/errors', 'plugin:import/warnings'],
  plugins: ['import', 'babel', 'prettier'],
  ignorePatterns: ['lib', '!webpack.config.js', 'packages/*/lib/*', 'node_modules', '**/__tests__/*'],
  rules: {
    'no-empty-function': 'off',
    'no-restricted-syntax': [
      'error',
      {
        selector: 'ExportNamedDeclaration',
        message: 'export default / export is not allowed, use module.exports instead',
      },
      {
        selector: 'ExportDefaultDeclaration',
        message: 'export default / export is not allowed, use module.exports instead',
      },
      {
        selector: 'ExportAllDeclaration',
        message: 'export default / export is not allowed, use module.exports instead',
      },
      {
        selector: 'ImportDeclaration',
        message: 'import is not allowed, use require() instead',
      },
      {
        selector: 'ImportDefaultSpecifier',
        message: 'import is not allowed, use require() instead',
      },
      {
        selector: 'ImportNamespaceSpecifier',
        message: 'import is not allowed, use require() instead',
      },
      {
        selector: 'ImportSpecifier',
        message: 'import is not allowed, use require() instead',
      },
    ],
  },
}
