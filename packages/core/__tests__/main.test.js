const { replaceMap } = require('../src')

let config, obj

describe('main', () => {
  config = {
    a: 'b.c.d',
    q: 'b.c.e',
  }

  obj = {
    b: {
      c: {
        d: 42,
        e: 12,
      },
    },
  }

  const obj2 = {
    b: {
      c: {
        e: 42,
      },
    },
  }

  const res = {
    a: [42],
    q: [12, 42],
  }

  it('happyPath', () => {
    expect(replaceMap(config, obj, obj2)).toEqual(res)
  })
})
