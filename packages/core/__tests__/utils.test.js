const { path, pathOr, objectGraph, allObjPaths, normalizePath } = require('../src/utils')

describe('path', () => {
  it('success with string keys', () => {
    const keys = 'a.b.c'
    const target = { a: { b: { c: 123 } } }

    expect(path(keys, target)).toEqual(123)
  })

  it('success with array keys', () => {
    const keys = ['a', 'b', 'c']
    const target = { a: { b: { c: 123 } } }

    expect(path(keys, target)).toEqual(123)
  })

  it('wrong path', () => {
    const keys = ['a', 's', 'c']
    const target = { a: { b: { c: 123 } } }

    expect(path(keys, target)).toEqual(undefined)
  })

  it('error: wrong keys type', () => {
    const keys = {}
    const target = { a: { b: { c: 123 } } }

    try {
      path(keys, target)
    } catch (e) {
      expect(e).toEqual(new Error('Wrong type!'))
    }
  })

  it('error: wrong target type', () => {
    const keys = 'a.b.c'
    const target = 'a.b.c=1'

    try {
      path(keys, target)
    } catch (e) {
      expect(e).toEqual(new Error('Wrong type!'))
    }
  })
})

describe('pathOr', () => {
  const defaultValue = 'deff'

  it('success with string keys', () => {
    const keys = 'a.b.c'
    const target = { a: { b: { c: 123 } } }

    expect(pathOr(keys, defaultValue, target)).toEqual(123)
  })

  it('success with array keys', () => {
    const keys = ['a', 'b', 'c']
    const target = { a: { b: { c: 123 } } }

    expect(pathOr(keys, defaultValue, target)).toEqual(123)
  })

  it('success with undefined last target value', () => {
    const keys = 'a.b.c'
    const target = { a: { b: { c: undefined } } }

    expect(pathOr(keys, defaultValue, target)).toEqual(undefined)
  })

  it('wrong path: defaultValue', () => {
    const keys = ['a', 's', 'c']
    const target = { a: { b: { c: 123 } } }

    expect(pathOr(keys, defaultValue, target)).toEqual(defaultValue)
  })

  it('error: wrong keys type', () => {
    const keys = {}
    const target = { a: { b: { c: 123 } } }

    try {
      pathOr(keys, defaultValue, target)
    } catch (e) {
      expect(e).toEqual(new Error('Wrong type!'))
    }
  })

  it('error: wrong target type', () => {
    const keys = 'a.b.c'
    const target = 'a.b.c=1'

    try {
      pathOr(keys, defaultValue, target)
    } catch (e) {
      expect(e).toEqual(new Error('Wrong type!'))
    }
  })
})

describe('objectGraph', () => {
  it('happyPath', () => {
    const data = {
      a: 2,
      b: {
        c: 3,
        d: [{ a: 2 }],
        e: {
          f: {
            g: 23,
          },
        },
      },
      z: 2,
    }

    expect(objectGraph(data)).toEqual({
      0: { root: 'root', data: ['a', 'b', 'z'] },
      1: { root: 'root.b', data: ['c', 'd', 'e'] },
      2: { root: 'root.b.e', data: ['f'] },
      3: { root: 'root.b.e.f', data: ['g'] },
    })
  })
})

describe('allObjPaths', () => {
  it('happyPath', () => {
    const data = {
      a: 2,
      b: {
        c: 3,
        d: [{ a: 2 }],
        e: {
          f: {
            g: 23,
          },
        },
      },
      z: 2,
    }

    expect(allObjPaths(data)).toEqual([
      'root.a',
      'root.b',
      'root.z',
      'root.b.c',
      'root.b.d',
      'root.b.e',
      'root.b.e.f',
      'root.b.e.f.g',
    ])
  })
})

describe('normalizePath', () => {
  it('removing root mark', () => {
    const data = ['root.a', 'root.b', 'root.z', 'root.b.c', 'root.b.d', 'root.b.e', 'root.b.e.f', 'root.b.e.f.g']

    expect(normalizePath(data)).toEqual(['a', 'b', 'z', 'b.c', 'b.d', 'b.e', 'b.e.f', 'b.e.f.g'])
  })
})
