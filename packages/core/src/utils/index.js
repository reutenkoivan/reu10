const path = (keys, target) => {
  if ((typeof keys !== 'string' && !Array.isArray(keys)) || !(target instanceof Object)) {
    throw new Error('Wrong type!')
  }

  if (typeof keys === 'string') {
    keys = keys.split('.')
  }

  return keys.reduce((res, key) => {
    if (res) {
      return res[key]
    }
  }, target)
}

const pathOr = (keys, defaultValue, target) => {
  if ((typeof keys !== 'string' && !Array.isArray(keys)) || !(target instanceof Object)) {
    throw new Error('Wrong type!')
  }

  if (typeof keys === 'string') {
    keys = keys.split('.')
  }

  const lastLevelKey = keys.pop()
  const lastLevelValue = path(keys, target)

  if (lastLevelValue && lastLevelKey in lastLevelValue) {
    return lastLevelValue[lastLevelKey]
  }

  return defaultValue
}

const objectGraph = (obj) => {
  const pathMap = {}
  let level = 0

  const helper = (smallObject, root) => {
    for (const key in smallObject) {
      pathMap[level] = Object.assign({ root, data: [] }, pathMap[level] || {})
      pathMap[level].data = [...pathMap[level].data, key]

      // в массивах пути не ищет
      if (typeof smallObject[key] === 'object' && !Array.isArray(smallObject[key])) {
        level++
        helper(smallObject[key], [pathMap[level - 1].root, key].join('.'))
        level--
      }
    }
  }

  helper(obj, 'root')

  return pathMap
}

const allObjPaths = (obj) => {
  const objData = objectGraph(obj)
  const result = []

  Object.values(objData).forEach(({ data, root }) => {
    for (const key of data) {
      result.push(`${root}.${key}`)
    }
  })

  return result
}

const normalizePath = (arr) => arr.map((p) => p.replace(/^root\./, ''))

module.exports = { path, pathOr, objectGraph, allObjPaths, normalizePath }
