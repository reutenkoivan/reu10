const { path, allObjPaths, normalizePath } = require('./utils')

const filterPath = (targetPath, targetData) => {
  return targetData.filter((p) => p === targetPath)
}

const resolvedPaths = (configPath, dataPath) => {
  return dataPath.map((targetData) => {
    return filterPath(configPath, targetData)
  })
}

const setValue = (resPath, data) => {
  return resPath.map((p, i) => {
    return p.map((q) => path(q, data[i]))
  })
}

const replaceMap = (config, ...data) => {
  const pathSet = data.map((o) => normalizePath(allObjPaths(o)))

  return Object.entries(config).reduce((res, [key, configPath]) => {
    const resPath = resolvedPaths(configPath, pathSet)
    res[key] = setValue(resPath, data).flat()

    return res
  }, {})
}

module.exports = { filterPath, resolvedPaths, setValue, replaceMap }
