const fs = require('fs')

module.exports = {
  process: function (_src, filename) {
    return fs.readFileSync(filename).toString()
  },
}
